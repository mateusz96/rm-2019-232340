#!/usr/bin/env python3
# Script for Roboty Mobilne laboratory by Mateusz Pietrzkowski
#	Task 1.
#	DONE: a) b)

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import random as rnd
import math


# Autogenerating flag (obstacles and Start&Finish point)
#	1- ON
#	0 - OFF
AUTOGENERATING = 1


# Size of table & grid
delta = 1
x_size = 10
y_size = 10


# Number of generating obstacles
num_of_obstacle = 4


# Generate obstacles
obst_vect = []
if AUTOGENERATING==1:
    for i in range(num_of_obstacle):
        x = rnd.uniform(-10, 10)
        y = rnd.uniform(-10, 10)
        tmp = ((x,y))
        obst_vect.append(tmp)
else:
    obst_vect.append((float(2.0001),float(9.0001)))
    obst_vect.append((float(-2.0001),float(-1.0001)))


# Parameters
k0 = 25
ko = []
for i in range(len(obst_vect)):
    ko.append(k0)
d0 = 40
kp = 15


# Generate of Start Point
if AUTOGENERATING==1:
    start_point_Y = rnd.randint(-10, 10)
    start_point=(-10,start_point_Y)
else:
    start_point=(-10,0)


# Generate of Finish Point
if AUTOGENERATING==1:
    finish_point_Y = rnd.randint(-10, 10)
    finish_point=(10,finish_point_Y)
else:
    finish_point=(10,5)


# Calculate Fo force
def CalculateFo(start):
    Fo = 0
    for i in range(len(obst_vect)):
        Norm = np.linalg.norm([start[0] - obst_vect[i][0], start[1] - obst_vect[i][1]])
        if Norm <= d0:
            Fo += -ko[i] * ((1/Norm) - (1/d0)) * (1/(Norm)**2)
        elif Norm > d0:
            Fo += 0
    return Fo


# Calculate Fp force
def CalculateFp(start):
    Fp = kp * np.linalg.norm([start[0] - finish_point[0], start[1] - finish_point[1]])
    return Fp


# Calculate F force (sum of Fp and Fo)
def CalculateF(Fp_, Fo_):
    F = Fp_ + Fo_
    return F


# Set grid
x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X**0)


# Calculate forces for every point of grid
Fi = []
for yi in range(len(y)):
    Fi_tmp = []
    for xi in range(len(x)):
        start = (x[xi], y[yi])
        Fpi = CalculateFp(start)
        Foi = CalculateFo(start)
        Fi_tmp.append(CalculateF(Fpi, Foi))
    Fi.append(Fi_tmp)


# Create the figure and set the title
fig = plt.figure(figsize=(8, 8))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjałów')


# Plot the force grid on the figure
plt.imshow(Fi, cmap=cm.RdYlGn, origin='lower', extent=[-x_size, x_size, -y_size, y_size])


# Plot start&finish points
plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')


# Plot obstacles
for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')


# Create the bar
plt.colorbar(orientation='vertical')


# Show the figure
plt.grid(True)
plt.show()
